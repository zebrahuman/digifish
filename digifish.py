#!/usr/bin/env python

import digitalocean
import requests
import json
import datetime
import pprint
import argparse
import prettytable

##
## List all droplets on account
##

def get_last_rebuild_time(droplet):
  try:
    last_touched =  filter(lambda x: x.type == 'rebuild', droplet.get_actions())[0].completed_at
  except IndexError:
    last_touched = "N/A";
  return last_touched

def parse_droplets(args):
  droplets = manager.get_all_droplets()
  if args.verbose:
    print "[INFO]: Listing available droplets"

  x = prettytable.PrettyTable(["id", "name", "image_id", "size_id", "region_id", "backups_active", "ip", "private_ip", "status", "created_at", "last_rebuilt"])
  x.padding_width = 1
  x.align["id"] = "l"
  x.align["name"] = "l"

  for d in droplets:
    x.add_row([d.id, d.name, "N/A", "N/A", "N/A", "N/A", d.ip_address, "N/A", d.status, d.created_at, get_last_rebuild_time(d)])
  print x

##
## Show droplet basing on id
##

def parse_show_droplet(args):
  d = manager.get_droplet(args.id)
  if args.verbose:
    print "[INFO]: Listing droplet information"
  if args.batch:
    if args.ip:
      print d.ip_address
    exit(0)
  else:
    x = prettytable.PrettyTable(["id", "name", "image_id", "size_id", "region_id", "backups_active", "ip", "private_ip", "status", "created_at", "last_rebuilt"])
    x.add_row([d.id, d.name, "N/A", "N/A", "N/A", "N/A", d.ip_address, "N/A", d.status, "N/A", "N/A"])
    print x

##
## Destroy a droplet
##

def parse_destroy_droplet(args):
  if args.verbose:
    print "[INFO]: Requesting droplet destroy..."
  try:
    d = manager.get_droplet(args.id)
    d.destroy()
    if args.verbose:
      print "[INFO]: Destroy request successful. Event id:", event_id
    elif args.batch:
      print event_id
    exit(0)
  except Exception as e:
    pass
    print "[FATAL]:", e
    exit(1)

##
## Create a new droplet
##

# XXX dop's implementation had a bug that disallowed adding ssh keys to the request
# that's why implemented it by hand

def parse_new_droplet(args):
  
  if args.verbose:
    print "[INFO]: Creating a new droplet..."
    print "Name: " , args.name
    print "Size_id: ", args.size_id
    print "Image_id: ", args.image_id
    print "Region_Id: ", args.region_id
    print "ssh_key_ids: ", args.ssh_key_ids
    print "private_networking: ", args.private_networking
    print "backups_enabled: ", args.backups_enabled


  ssh_key_objs = []
  for key_id in args.ssh_key_ids.split(','):
    ssh_key_objs.append(manager.get_ssh_key(key_id))

  droplet = digitalocean.Droplet(token=args.token,
                               name=args.name,
                               region=args.region_id,
                               image=args.image_id,
                               size_slug=args.size_id,
                               ssh_keys=ssh_key_objs,
                               private_networking=args.private_networking,
                               backups=args.backups_enabled)
  try:
    droplet.create()
    create_action_id = droplet.action_ids[0]
  except Exception as e:
    print "[FATAL]:", e
    exit(1)

  if args.verbose:
    print "Droplet id: {id}. Action id: {action_id}".format(id=droplet.id, action_id=create_action_id)
    print "Droplet creation command sent. It will take ~60 seconds for the droplet to be available. Use show-event to query the status of the action using the action id value"
  elif args.batch:
    print create_action_id
    exit(0)
  else:
    print "Droplet id: {id}. Action id: {action_id}".format(id=droplet.id, action_id=create_action_id)

##
## Get information about an event
##


def parse_show_event(args):
  if args.verbose:
    print "[INFO]: Requesting information about event:", args.id
  try:
    action_obj = manager.get_action(args.id)
  except Exception as e:
    print "[FATAL]:", e
    exit(1)

  if args.batch:
    print action_obj.status
  else:
    print "{type}, {status}, {completed_at}".format(type=action_obj.type,status=action_obj.status,completed_at=action_obj.completed_at)
    exit(0)


##
## List ssh keys on account
##

def parse_ssh_keys(args):
  keys = manager.get_all_sshkeys()
  if args.verbose:
    print "[INFO]: Listing ssh keys"
  x = prettytable.PrettyTable(["id", "name"])
  for k in keys:
    x.add_row([k.id, k.name])
  print x


##
## List images on the account
##

def parse_images(args):
  images = manager.get_images()
  if args.verbose:
    print "[INFO]: Listing images"
  x = prettytable.PrettyTable(["id", "name", "distro", "min disk size", "regions", "created_at", "public", "size gbs"])
  for im in images:
    x.add_row([im.id, im.name, im.distribution, im.min_disk_size, im.regions, im.created_at, im.public, 'N/A'])
  print x


##
## Rebuild a droplet
##

def parse_rebuild_droplet(args):
  try:
    d = manager.get_droplet(args.id)
    ret = d.rebuild(image_id = args.image_id)
  except Exception as e:
    print "[FATAL]:", e
    exit(1)

  if args.verbose:
    print "Rebuild initiated with args: Image id: {image_id}.".format(image_id=args.image_id)
    print "Returned dict: {dictio}".format(ret)
  if args.batch:
    print ret['action']['id']
    exit(0)
  else:
    print "Rebuild initiated. Action id: {a_id}".format(a_id=ret['action']['id'])

parser = argparse.ArgumentParser(description="Digital Ocean droplet management")

group = parser.add_mutually_exclusive_group()
group.add_argument("-v", "--verbose", help = "Turn on verbose output", action = "store_true")
group.add_argument("-b", "--batch", help = "Turn on scripting support", action="store_true")

parser.add_argument("--token", help = "Digital ocean token", required = True)

subparsers = parser.add_subparsers(help='sub-command help')

parser_droplets = subparsers.add_parser("droplets", help = "Lists droplets")
parser_droplets.set_defaults(func=parse_droplets)

parser_show_droplet = subparsers.add_parser("show-droplet", help = "Shows droplet based on droplet id")
parser_show_droplet.add_argument("-i", "--id", help = "Required, Numeric, this is the id of the droplet you want to show", required = True)
parser_show_droplet.add_argument("-a", "--ip", help = "Optional, Boolean, displays ip infromation only for your droplet", action = "store_true")
parser_show_droplet.set_defaults(func=parse_show_droplet)

parser_new_droplet = subparsers.add_parser("new-droplet", help = "Creates a new droplet")
parser_new_droplet.add_argument("-n", "--name", help = "Required, String, this is the name of the droplet - must be formatted by hostname rules", required = True)
parser_new_droplet.add_argument("-s", "--size_id", help = "Required, Numeric, this is the id of the size you would like the droplet created at", required = True)
parser_new_droplet.add_argument("-i", "--image_id", help = "Required, Numeric, this is the id of the image you would like the droplet created with", required = True)
parser_new_droplet.add_argument("-r", "--region_id",  help = "Required, Numeric, this is the id of the region you would like your server in", required = True)
parser_new_droplet.add_argument("-k", "--ssh_key_ids", help = "NOT SUPPORTED Optional, Numeric CSV, comma separated list of ssh_key_ids that you would like to be added to the server")
parser_new_droplet.add_argument("-p", "--private_networking", help = "Optional, Boolean, enables a private network interface if the region supports private networking", action = "store_true")
parser_new_droplet.add_argument("-b", "--backups_enabled", help = "Optional, Boolean, enables backups for your droplet", action = "store_true")
parser_new_droplet.add_argument("-v", "--virtio", help = "Optional, Boolean, enables virtio for your droplet", action = "store_true")
parser_new_droplet.add_argument("-d", "--dry_run", help = "Optional, Boolean, print the execution plan without doing actual changes", action = "store_true")
parser_new_droplet.set_defaults(func=parse_new_droplet)

parser_reboot_droplet = subparsers.add_parser("rebuild-droplet", help = "Rebuild a droplet")
parser_reboot_droplet.add_argument("-i", "--id", help = "Required, Numeric, this is the id of the droplet you want to rebuild", required = True)
parser_reboot_droplet.add_argument("-r", "--image_id", help = "Optional, Numeric, this is the id of the image to use when rebuilding. If ommited, droplet's current image is used")
parser_reboot_droplet.set_defaults(func=parse_rebuild_droplet)

#parser_reboot_droplet = subparsers.add_parser("reboot-droplet", help = "Reboot a droplet")
#parser_reboot_droplet.add_argument("-i", "--id", help = "Required, Numeric, this is the id of the droplet you want to reboot", required = True)
#parser_reboot_droplet.set_defaults(func=parse_reboot_droplet)
#
#parser_shutdown_droplet = subparsers.add_parser("shutdown-droplet", help = "Shutdown a droplet")
#parser_shutdown_droplet.add_argument("-i", "--id", help = "Required, Numeric, this is the id of the droplet you want to shutdown", required = True)
#parser_shutdown_droplet.set_defaults(func=parse_shutdown_droplet)
#
parser_destroy_droplet = subparsers.add_parser("destroy-droplet", help = "Destroy a droplet")
parser_destroy_droplet.add_argument("-i", "--id", help = "Required, Numeric, this is the id of the droplet you want to destroy", required = True)
parser_destroy_droplet.add_argument("-s", "--scrub", help = "NOT SUPPORTED Optional, Boolean, enables prior scrubing of the droplet you want to destroy", action = "store_true")
parser_destroy_droplet.set_defaults(func=parse_destroy_droplet)

parser_ssh_keys = subparsers.add_parser("ssh-keys", help = "Lists ssh keys")
parser_ssh_keys.set_defaults(func=parse_ssh_keys)

parser_ssh_keys = subparsers.add_parser("images", help = "Lists all images")
parser_ssh_keys.set_defaults(func=parse_images)

#parser_regions = subparsers.add_parser("regions", help = "Lists regions")
#parser_regions.set_defaults(func=parse_regions)

parser_show_event = subparsers.add_parser("show-event", help = "Shows event based on an action id")
parser_show_event.add_argument("-i", "--id", help = "Required, Numeric, this is the id of the event you want to show", required = True)
parser_show_event.add_argument("-d", "--droplet_id", help = "Optional, Boolean, print the droplet id associated with this event", action = "store_true")
parser_show_event.set_defaults(func=parse_show_event)

args = parser.parse_args()
if args.verbose:
  print "[INFO]: Verbose output is ON"



manager = digitalocean.Manager(token=args.token)
args.func(args)

